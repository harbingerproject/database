<?php

/**
 * This file is part of Harbinger Project.
 *
 * Copyright (c) 2015, Gabriel Heming <gabriel.heming@hotmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Gabriel Heming nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gabriel Heming <gabriel.heming@hotmail.com>
 * @copyright 2015 Gabriel Heming. All rights reserved.
 * @license http://www.opensource.org/licenses/bsd-license.php BSD License
 **/

namespace Harbinger\Database;

use \Harbinger\Iterator\Filter;

/**
 * Handler for drivers connections
 * @package Harbinger
 * @subpackage Database
 * @author Gabriel Heming <gabriel.heming@hotmail.com>
 **/
class Manager
{

    /**
     * @var \Harbinger\Database\ConnectorFactory
     * @see \Harbinger\Database\Manager::__construct() initiate the connector factory
     **/
    private $connectorFactory;

    /**
     * @var \Harbinger\Database\Connector\Collection\Driver
     * @see \Harbinger\Database\Manager::__construct() Initiate the driver collection
     * @see \Harbinger\Database\Manager::setDriverCollection() Define the driver collection
     * @see \Harbinger\Database\Manager::getDriverCollection() Retrieve the driver collection
     **/
    private $driverCollection;

    /**
     * Instantiate connector factory and driver collection
     * @param \Harbinger\Database\Connector\Collection\Driver $driverCollection
     **/
    public function __construct($driverCollection = null)
    {
        $this->connectorFactory = new ConnectorFactory();
        $this->setDriverCollection(!is_null($driverCollection) ? $driverCollection : new Connector\Collection\Driver());
    }

    /**
     * @param \Harbinger\Database\Connector\Collection\Driver $driverCollection
     **/
    public function setDriverCollection(Connector\Collection\Driver $driverCollection) {
        $this->driverCollection = $driverCollection;
    }

    /**
     * @return \Harbinger\Database\Connector\Collection\Driver
     **/
    public function getDriverCollection()
    {
        return $this->driverCollection;
    }

    /**
     * @param string $key
     * @return \Harbinger\Database\Connector
     * @throws \Harbinger\Database\OutOfBoundsException If there's no driver for specified key
     **/
    public function getConnector($key)
    {
        $callback = function (\Harbinger\Database\Connector\Driver $driver) use ($key) {
            return $driver->getKey() == $key;
        };

        foreach (new \CallbackFilterIterator($this->getDriverCollection() , $callback) as $driver) {
            return $this->connectorFactory->create($driver->getKey() , $driver);
        }

        throw new OutOfBoundsException(sprintf('Driver not specified for key: %s' , $key));
    }
}
