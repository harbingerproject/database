<?php

/**
 * This file is part of Harbinger Project.
 *
 * Copyright (c) 2015, Gabriel Heming <gabriel.heming@hotmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Gabriel Heming nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gabriel Heming <gabriel.heming@hotmail.com>
 * @copyright 2015 Gabriel Heming. All rights reserved.
 * @license http://www.opensource.org/licenses/bsd-license.php BSD License
 **/

namespace Harbinger\Database\Statement;

use Harbinger\Database\Connector;
use Harbinger\Database\UnexpectedValueException;

/**
 * Interface for a statement
 * @author Gabriel Heming <gabriel.heming@hotmail.com>
 * @package Harbinger
 * @subpackage Database\Statement
 **/
abstract class MySQLiStatement extends AbstractStatement
{

    /**
     * @see http://php.net/manual/en/class.mysqli-stmt.php Documentation of MySQLi Statement
     *
     * @var \mysqli_stmt
     **/
    protected $statement;

    /**
     * @see \Harbinger\Database\Statement\MySQLiStatement::__construct()
     * @see \Harbinger\Database\Statement\MySQLiStatement::bindValue()
     * @var mixed[]
     **/
    private $parameters = array();

    /**
     * @param string $query
     * @param mysqli $mysqli
     * @see http://php.net/manual/en/class.mysqli.php Documentation of MySQLi
     **/
    public function __construct($query , \mysqli $mysqli)
    {
        $this->statement = $mysqli->prepare($this->normalizeQuery($query));
    }

    private function normalizeQuery($query)
    {
        $data = array(0 => array());

        preg_match_all('/\:\w+/' , $query , $data);

        foreach ($data[0] as $parameter) {
            $this->parameters[$parameter] = null;
        }

        return preg_replace('/\:\w+/' , '?' , $query);
    }

    /**
     * {@inheritdoc}
     **/
    public function bindValue($parameter , $value , $type = Connector::PARAM_STRING)
    {
        $type = $this->getType($type);

        if ((int)$parameter > 0 && (int)$parameter == $parameter) {
            $this->parameters[$parameter] = array('type' => $type , 'value' => $value);
        }

        if (!array_key_exists($parameter , $this->parameters)) {
            throw new UnexpectedValueException(
                sprintf('Parameter %s didn\'t match with position or named placehodler' , $parameter)
            );
        }

        $this->parameters[$parameter] = array('type' => $type , 'value' => $value);
    }

    /**
     * return the specified type for MySQLi Statement
     * @param int $type
     * @return string
     **/
    private function getType($type)
    {
        switch ($type) {
            case Connector::PARAM_BOOLEAN:
            case Connector::PARAM_INTEGER:
                return 'i';
                break;
            case Connector::PARAM_DOUBLE:
                return 'd';
                break;
            case Connector::PARAM_LOB:
                return 'b';
                break;
            case Connector::PARAM_NULL:
            case Connector::PARAM_STRING:
            default:
                return 's';
        }
    }

    /**
     * {@inheritdoc}
     **/
    public function close()
    {
        return $this->statement->close();
    }

    /**
     * {@inheritdoc}
     **/
    public function execute()
    {
        $type = '';

        $values = array();

        foreach ($this->parameters as $parameter) {
            $type .= $parameter['type'];
            $values[] = &$parameter['value'];
        }

        array_unshift($values , $type);

        if (count($values) > 1) {
            $reflectionMethod = new \ReflectionMethod($this->statement , 'bind_param');
            $reflectionMethod->invokeArgs($this->statement , $values);
        }

        return $this->statement->execute();
    }
}
