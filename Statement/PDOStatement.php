<?php

/**
 * This file is part of Harbinger Project.
 *
 * Copyright (c) 2015, Gabriel Heming <gabriel.heming@hotmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Gabriel Heming nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gabriel Heming <gabriel.heming@hotmail.com>
 * @copyright 2015 Gabriel Heming. All rights reserved.
 * @license http://www.opensource.org/licenses/bsd-license.php BSD License
 **/

namespace Harbinger\Database\Statement;

use Harbinger\Database\Connector;

/**
 * Interface for a statement
 * @author Gabriel Heming <gabriel.heming@hotmail.com>
 * @package Harbinger
 * @subpackage Database\Statement
 **/
class PDOStatement extends AbstractStatement
{

    /**
     * @see http://php.net/manual/en/class.pdostatement.php Documentation of PDOStatement
     *
     * @var \PDOStatement
     **/
    private $statement;

    /**
     * @param \PDOStatement $statement
     * @see http://php.net/manual/en/class.pdostatement.php Documentation of PDOStatement
     **/
    public function __construct(\PDOStatement $statement)
    {
        $this->statement = $statement;
    }

    /**
     * {@inheritdoc}
     **/
    public function bindValue($parameter , $value , $type = Connector::PARAM_STRING)
    {
        $this->statement->bindValue($parameter , $value , $this->getType($type));
    }

    /**
     * return the specified type for PDOStatement
     * @param int $type
     * @return int
     **/
    private function getType($type)
    {
        switch ($type) {
            case Connector::PARAM_BOOLEAN:
                return \PDO::PARAM_BOOL;
                break;
            case Connector::PARAM_DOUBLE:
            case Connector::PARAM_INTEGER:
                return \PDO::PARAM_INT;
                break;
            case Connector::PARAM_LOB:
                return \PDO::PARAM_LOB;
                break;
            case Connector::PARAM_NULL:
                return \PDO::PARAM_NULL;
                break;
            case Connector::PARAM_STRING:
            default:
                return \PDO::PARAM_STR;
        }
    }

    /**
     * {@inheritdoc}
     **/
    public function close()
    {
        return $this->statement->closeCursor();
    }

    /**
     * {@inheritdoc}
     **/
    public function fetch()
    {
        return $this->statement->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     * {@inheritdoc}
     **/
    protected function doFetchAll()
    {
        return $this->statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * {@inheritdoc}
     **/
    public function execute()
    {
        return $this->statement->execute();
    }
}
