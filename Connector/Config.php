<?php

/**
 * This file is part of Harbinger Project.
 *
 * Copyright (c) 2015, Gabriel Heming <gabriel.heming@hotmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Gabriel Heming nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gabriel Heming <gabriel.heming@hotmail.com>
 * @copyright 2015 Gabriel Heming. All rights reserved.
 * @license http://www.opensource.org/licenses/bsd-license.php BSD License
 **/

namespace Harbinger\Database\Connector;

/**
 * Class with configuration for a database connector
 * @package Harbinger
 * @subpackage Database\Connector
 * @author Gabriel Heming <gabriel.heming@hotmail.com>
*/
class Config
{

    /**
     * @var string
     */
    private $host;

    /**
     * @var string
     */
    private $baseName;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $user;

    /**
     * @var int
     */
    private $port;

    /**
     * Instantiate a config connector
     * @param string $host
     * @param string $name
     * @param string $user
     * @param string $password
     */
    public function __construct($host , $baseName , $user = null , $password = null , $port = null)
    {
        $this->host = (string)$host;
        $this->baseName = (string)$baseName;
        $this->user = (string)$user;
        $this->password = (string)$password;
        $this->port = ((int)$port > 0 && (int)$port == $port) ? (int)$port : null;
    }

    /**
     * Retrieve the host's name
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * Retrieve the password for connection
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Retrieve the base name
     * @return string
     */
    public function getBaseName()
    {
        return $this->baseName;
    }

    /**
     * retrieve the user for connection
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Retrieve the port for connection
     * @return int
     */
    public function getPort()
    {
        return $this->port;
    }
}
