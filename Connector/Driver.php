<?php

/**
 * This file is part of Harbinger Project.
 *
 * Copyright (c) 2015, Gabriel Heming <gabriel.heming@hotmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Gabriel Heming nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gabriel Heming <gabriel.heming@hotmail.com>
 * @copyright 2015 Gabriel Heming. All rights reserved.
 * @license http://www.opensource.org/licenses/bsd-license.php BSD License
 **/

namespace Harbinger\Database\Connector;

use \Harbinger\Database\ConnectorFactory;
use \Harbinger\Database\OutOfBoundsException;

/**
 * Class which represents a driver connector
 * @package Harbinger
 * @subpackage Database\Connector
 * @author Gabriel Heming <gabriel.heming@hotmail.com>
*/
class Driver
{
    /**
     * @var string
     **/
    private $key;

    /**
     * @var string
     **/
    private $sgbd;

    /**
     * @var \Harbinger\Database\Connector\Config
     **/
    private $config;

    /**
     * key = value
     * @var mixed[]
     **/
    private $options = [];

    /**
     * @param string $key
     * @param string $sgdb
     * @param \Harbinger\Database\Connector\Config $config
     * @param array $options
     * @throws \Harbinger\Database\OutOfBoundsException if the sgbd isn't supported
     **/
    public function __construct($key , $sgdb , Config $config , array $options = [])
    {
        $this->setKey($key);
        $this->setSgbd($sgdb);
        $this->setConfig($config);

        if (isset($options)) {
            $this->setOptions($options);
        }
    }

    /**
     * @param string $key
     **/
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @param string $sgdb
     * @throws \Harbinger\Database\OutOfBoundsException if the sgbd isn't supported
     **/
    public function setSgbd($sgdb)
    {
        $this->sgbd = $sgdb;
    }

    /**
     * @param \Harbinger\Database\Connector\Config $config
     **/
    public function setConfig(Config $config)
    {
        $this->config = $config;
    }

    /**
     * @param array $options
     **/
    public function setOptions(array $options)
    {
        $this->options = array_merge($this->options , $options);
    }

    /**
     * @return string
     **/
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @return string
     **/
    public function getSgbd()
    {
        return $this->sgbd;
    }

    /**
     * @return \Harbinger\Database\Connector\Config
     **/
    public function getConfig()
    {
        return $this->config;
    }


    /**
     * @return array
     **/
    public function getOptions()
    {
        return $this->options;
    }
}
