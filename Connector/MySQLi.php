<?php

/**
 * This file is part of Harbinger Project.
 *
 * Copyright (c) 2015, Gabriel Heming <gabriel.heming@hotmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Gabriel Heming nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gabriel Heming <gabriel.heming@hotmail.com>
 * @copyright 2015 Gabriel Heming. All rights reserved.
 * @license http://www.opensource.org/licenses/bsd-license.php BSD License
 **/

namespace Harbinger\Database\Connector;

use \Harbinger\Database\RuntimeException;
use \Harbinger\Database\Statement\MySQLiStatement;

/**
 * abstract for a PDO connector
 * @package Harbinger
 * @subpackage Database\Connector
 * @author Gabriel Heming <gabriel.heming@hotmail.com>
 **/
class MySQLi implements \Harbinger\Database\Connector
{

    /**
     * @var \mysqli
     * @see http://php.net/manual/en/class.mysqli.php MySQLi documentation
     **/
    private $mysqli;

    /**
     * construct the MySQLi connector based by connector config
     * @param \Harbinger\Database\Connector\Config $connectorConfig
     * @throws \Harbinger\Database\RuntimeException If connection could not be establish
     **/
    public function __construct(\Harbinger\Database\Connector\Config $connectorConfig)
    {
        try {
            mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

            $this->mysqli = new \mysqli(
                $connectorConfig->getHost(),
                $connectorConfig->getUser(),
                $connectorConfig->getPassword(),
                $connectorConfig->getBasename(),
                $connectorConfig->getPort()
            );
        } catch (\mysqli_sql_exception $exception) {
            throw new RuntimeException(
                'Could not establish a database connection.',
                $exception->getCode(),
                $exception
            );
        }
    }

    /**
     * {@inheritdoc}
     **/
    public function prepare($query)
    {
        if (function_exists('mysqli_fetch_all')) {
            return new MySQLiStatement\NativeDriver($query , $this->mysqli);
        }

        return new MySQLiStatement\ClientLibrary($query , $this->mysqli);
    }
}
