<?php

/**
 * This file is part of Harbinger Project.
 *
 * Copyright (c) 2015, Gabriel Heming <gabriel.heming@hotmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Gabriel Heming nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gabriel Heming <gabriel.heming@hotmail.com>
 * @copyright 2015 Gabriel Heming. All rights reserved.
 * @license http://www.opensource.org/licenses/bsd-license.php BSD License
 **/

namespace Harbinger\Database\Connector\PDO;

use Harbinger\Database\OutOfBoundsException;

/**
 * @package Harbinger
 * @subpackage Database
 * @author Gabriel Heming <gabriel.heming@hotmail.com>
 **/
class DSNFactory
{
    /**
     * Define the as DBLIB connector
     * @var string
     **/
    const DBLIB = 'DBLIB';

    /**
     * Define the as MySQL connector
     * @var string
     **/
    const MYSQL = 'MySQL';

    /**
     * Define the as MsSQL connector
     * @var string
     **/
    const MSSQL = 'MsSQL';

    /**
     * Define the as DBLIB connector
     * @var string
     **/
    const ODBC = 'ODBC';

    /**
     * Define the as PostgreSQL connector
     * @var string
     **/
    const PGSQL = 'PgSQL';

    /**
     * Define the as SQLite connector
     * @var string
     **/
    const SQLITE = 'SQLite';

    /**
     * Define the as SQLSRV connector
     * @var string
     **/
    const SQLSRV = 'SQLSRV';

    /**
     * @param \Harbinger\Database\Connector\Driver $driver
     * @return \Harbinger\Database\PDO\DSN $dsn
     **/
    public function create(\Harbinger\Database\Connector\Driver $driver)
    {
        $sgbd = strtoupper($driver->getSgbd());

        if ($sgbd === self::MSSQL) {
            return $this->createMsSQLDSN($driver);
        }

        $reflectionClass = new \ReflectionClass($this);

        if ($reflectionClass->getConstant($sgbd) !== false) {
            $class = DSN::class.'\\'.$reflectionClass->getConstant($sgbd);

            return new $class($driver);
        }

        throw new OutOfBoundsException(sprintf('Not found connector for the specified sgdb: %s' , $sgbd));
    }

    private function createMsSQLDSN(\Harbinger\Database\Connector\Driver $driver)
    {
        $drivers = \PDO::getAvailableDrivers();

        if (in_array('sqlsrv' , $drivers)) {
            return new DSN\SQLSRV($drivers);
        }

        if (in_array('dblib' , $drivers)) {
            return new DSN\DBLIB($drivers);
        }

        if (in_array('odbc' , $drivers)) {
            $options = $drivers>getOptions();

            if (isset($options['cataloged-name'])) {
                return new DSN\ODBC\Cataloged($drivers);
            }

            if (isset($options['drive-name'])) {
                return new DSN\ODBC\Unataloged($drivers);
            }

            throw new RuntimeException(
                sprintf("There's no drive for MsSQL.\nAvailable drivers: %s" , implode('/' , $drivers))
            );
        }

        throw new RuntimeException(
            sprintf("There's no drive for MsSQL.\nAvailable drivers: %s" , implode('/' , $drivers))
        );
    }
}
