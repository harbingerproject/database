<?php

namespace Harbinger\Database\Tests\Connector;

use \PHPUnit_Framework_TestCase as PHPUnit;
use \Harbinger\Database\Connector\Driver;

class DriverTest extends PHPUnit {

    public function assertPreConditions() {
        $this->assertTrue(class_exists($class = Driver::class) , 'Class not found: '.$class);
    }

    public function setUp() {}

    public function testCreateConnectorDriver() {
        $stub = $this->getMockBuilder(\Harbinger\Database\Connector\Config::class)
                     ->disableOriginalConstructor()
                     ->getMock();

        $key = 'default-connection';
        $sqbd = 'mysql';

        return new \Harbinger\Database\Connector\Driver($key , $sqbd , $stub);
    }

    /**
     * @depends testCreateConnectorDriver
     **/
    public function testConnectorDriverMethodsWhatShouldExists($config) {
        $methods[] = 'setKey';
        $methods[] = 'setSgbd';
        $methods[] = 'setConfig';
        $methods[] = 'getKey';
        $methods[] = 'getSgbd';
        $methods[] = 'getConfig';

        foreach($methods AS $method) {
            $this->assertTrue(method_exists($config , $method) , sprintf('Method %s not found' , $method));
        }
    }
}
