<?php

namespace Harbinger\Database\Tests\Connector\PDO;

use \PHPUnit_Framework_TestCase as PHPUnit;
use \Harbinger\Database\Connector\PDO\DSN;

class DSNTest extends PHPUnit {

    public function assertPreConditions() {
        $this->assertTrue(class_exists($class = DSN::class) , 'Class not found: '.$class);
    }

    public function setUp() {
        //$this->stub = $this->get(DSN::class);

        $this->stub = $this->getMockBuilder(DSN::class)
                           ->disableOriginalConstructor()
                           ->getMock();
    }

    public function testPDODSNMethodsWhatShouldExists() {
        $reflection = new \ReflectionClass(DSN::class);

        $methods[] = 'getDriver';
        $methods[] = '__toString';
        $methods[] = 'getDSN';

        foreach($methods AS $method) {
            $this->assertTrue(method_exists($this->stub , $method) , sprintf('Method %s not found' , $method));
        }
    }
}
