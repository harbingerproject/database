<?php

namespace Harbinger\Database\Tests\Connector;

use \PHPUnit_Framework_TestCase as PHPUnit;
use \Harbinger\Database\Connector\Config;

class ConfigTest extends PHPUnit {

    public function assertPreConditions() {
        $this->assertTrue(class_exists($class = Config::class) , 'Class not found: '.$class);
    }

    public function setUp() {}

    public function testCreateConnectorConfig() {
        $sgbd['host'] = 19216801;
        $sgbd['base_name'] = 54321;
        $sgbd['user'] = 54321;
        $sgbd['password'] = 54321;
        $sgbd['port'] = '0';

        $config = new Config($sgbd['host'] , $sgbd['base_name'] , $sgbd['user'] , $sgbd['password'] , $sgbd['port']);

        $this->assertSame((string)$sgbd['host'] , $config->getHost());
        $this->assertSame((string)$sgbd['base_name'] , $config->getBasename());
        $this->assertSame((string)$sgbd['user'] , $config->getUser());
        $this->assertSame((string)$sgbd['password'] , $config->getPassword());
        $this->assertSame(null , $config->getPort());
    }
}
