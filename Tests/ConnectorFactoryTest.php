<?php

namespace Harbinger\Database\Tests;

use \PHPUnit_Framework_TestCase as PHPUnit;
use \Harbinger\Database\ConnectorFactory;

class ConnectorFactoryTest extends PHPUnit {

    public function assertPreConditions() {
        $this->assertTrue(class_exists($class = ConnectorFactory::class) , 'Class not found: '.$class);
    }

    public function setUp() {}

    public function testConnectorFactoryMethodsWhatShouldExists() {
        $databaseManager = new ConnectorFactory();

        $methods[] = 'create';
        $methods[] = 'createConnector';
        $methods[] = 'has';
        $methods[] = 'get';

        foreach($methods AS $method) {
            $this->assertTrue(method_exists($databaseManager , $method) , sprintf('Method %s not found' , $method));
        }
    }

    public function testCreateConnectorFactory() {
        $connectorFactory = new ConnectorFactory();

        $this->assertInstanceOf(\Harbinger\Database\ConnectorFactory::class , $connectorFactory);

        return $connectorFactory;
    }

    /**
     * @depends testCreateConnectorFactory
     * @expectedException \Harbinger\Database\OutOfBoundsException
     **/
    public function testMethodcreateWithAnWrongKeyShouldThrowAnException($connectorFactory) {
        $stub = $this->getMockBuilder(\Harbinger\Database\Connector\Driver::class)
                     ->disableOriginalConstructor()
                     ->getMock();

        $stub->expects($this->any())
              ->method('getSgbd')
              ->willReturn('something');

        $connectorFactory->create('test' , $stub);
    }

    public function tearDown() {

    }
}
