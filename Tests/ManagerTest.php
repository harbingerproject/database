<?php

namespace Harbinger\Database\Tests;

use \PHPUnit_Framework_TestCase as PHPUnit;
use \Harbinger\Database\Manager;
use \Harbinger\Database;

class ManagerTest extends PHPUnit {

    public function assertPreConditions() {
        $this->assertTrue(class_exists($class = Manager::class) , 'Class not found: '.$class);
    }

    public function setUp() {}

    public function testManagerMethodsWhatShouldExists() {
        $databaseManager = new Manager();

        $methods[] = 'setDriverCollection';
        $methods[] = 'getDriverCollection';
        $methods[] = 'getConnector';

        foreach($methods AS $method) {
            $this->assertTrue(method_exists($databaseManager , $method) , sprintf('Method %s not found' , $method));
        }
    }

    /**
     * @testdox Create a instance of \Harbinger\Database\Manager to manager all database connections
     **/
    public function testCreateDatabaseManager() {
        $databaseManager = new Manager();

        $this->assertInstanceOf(\Harbinger\Database\Manager::class , $databaseManager);

        return $databaseManager;
    }

    public function testMethodSetAndGetDriverCollectionShouldWork() {
        $databaseManager = new Manager($driverCollection = new Database\Connector\Collection\Driver());
        $this->assertSame($driverCollection , $databaseManager->getDriverCollection());

        $databaseManager->setDriverCollection($newDriverCollection = new Database\Connector\Collection\Driver());
        $this->assertSame($newDriverCollection , $databaseManager->getDriverCollection());
        $this->assertNotSame($driverCollection , $databaseManager->getDriverCollection());
    }

    /**
     * @depends testCreateDatabaseManager
     * @expectedException \Harbinger\Database\OutOfBoundsException
     **/
    public function testMethodGetConnectorWithAnWrongKeyShouldThrowAnException($databaseManager) {
        $databaseManager->getConnector('test');
    }

    public function tearDown() {

    }
}
