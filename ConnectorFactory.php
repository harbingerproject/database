<?php

/**
 * This file is part of Harbinger Project.
 *
 * Copyright (c) 2015, Gabriel Heming <gabriel.heming@hotmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Gabriel Heming nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gabriel Heming <gabriel.heming@hotmail.com>
 * @copyright 2015 Gabriel Heming. All rights reserved.
 * @license http://www.opensource.org/licenses/bsd-license.php BSD License
 **/

namespace Harbinger\Database;

use Harbinger\Database\Connector\Driver;
use Harbinger\Database\Connector\MySQLi;
use Harbinger\Database\Connector\PDO;

/**
 * Factory responsibles by create the correct connector
 * @package Harbinger
 * @subpackage Database
 * @author Gabriel Heming <gabriel.heming@hotmail.com>
 **/
class ConnectorFactory
{

    /**
     * @var \ArrayIterator
     * @link http://php.net/manual/en/class.arrayiterator.php ArrayIterator documentation
     **/
    private static $connectorList;

    /**
     * Instantiate the connector factory
     **/
    public function __construct()
    {
        if (!self::$connectorList instanceof \Iterator) {
            self::$connectorList = new \ArrayIterator();
        }
    }

    /**
     * Create and return a connector,
     * if the $key corresponds an already created connector, return the existents connector
     * @param string $key
     * @param string $driver Driver specified by ConnectorFactory constants
     * @param \Harbinger\Database\Connector\Config $connectorConfig
     * @return \Harbinger\Database\Connector
     * @throws \Harbinger\Database\OutOfBoundsException If the specified driver is not found
     **/
    public function create($key , Driver $driver)
    {
        if (!$this->has($key)) {
            self::$connectorList->offsetSet($key , $this->createConnector($driver));
        }

        return $this->get($key);
    }

    /**
     * Retrieve a connector
     * @param string $key
     * @return \Harbinger\Database\Connector
     * @throws \OutOfBoundsException If the specified key is not valid
     **/
    public function get($key)
    {
        return self::$connectorList->offsetGet($key);
    }

    /**
     * Check if connector exists
     * @param string $key
     * @return boolean
     **/
    public function has($key)
    {
        return self::$connectorList->offsetExists($key);
    }

    /**
     * @param \Harbinger\Database\Connector\Driver $driver
     * @return \Harbinger\Database\Connector
     * @throws \Harbinger\Database\OutOfBoundsException If the specified driver is not found
     **/
    protected function createConnector(Driver $driver)
    {
        if (class_exists('\PDO')) {
            return $this->createPDOConnector($driver);
        }

        $sgbd = strtoupper($driver->getSgbd());

        if ($sgbd == 'MYSQL' && class_exists('\mysqli')) {
            return new MySQLi($driver->getConfig());
        }

        throw new OutOfBoundsException(sprintf('Not found connector for the specified sgdb: %s' , $sgbd));
    }

    protected function createPDOConnector(Driver $driver)
    {
        $dsnFactory = new PDO\DSNFactory();
        $dsn = $dsnFactory->create($driver);

        return new PDO($driver->getConfig() , $dsn);
    }
}
